FROM ubuntu:20.04

LABEL maintainer="ToM  <tom@leloop.org>"

ENV DEBIAN_FRONTEND noninteractive

# hadolint ignore=DL3008
RUN apt-get update --quiet --quiet \
    && apt-get install --assume-yes --quiet --quiet --no-install-recommends \
        sqlitebrowser \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY sqlitebrowser.conf /root/.config/sqlitebrowser/sqlitebrowser.conf

WORKDIR /sqlite
CMD ["sqlitebrowser"]
